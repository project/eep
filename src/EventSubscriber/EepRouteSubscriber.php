<?php

namespace Drupal\eep\EventSubscriber;

use Drupal\Core\Routing\RouteSubscriberBase;
use Drupal\Core\Routing\RoutingEvents;
use Symfony\Component\Routing\RouteCollection;
use Drupal\eep\EepManagerInterface;

/**
 * EMail enumeration prevention route subscriber.
 */
class EepRouteSubscriber extends RouteSubscriberBase {

  /**
   * The email enumaration prevention manager service.
   *
   * @var \Drupal\eep\EepManagerInterface
   */
  protected $eepManager;

  /**
   * Constructs a new EepRouteSubscriber.
   *
   * @param \Drupal\eep\EepManagerInterface $eep_manager
   *   The email enumaration prevention manager service.
   */
  public function __construct(EepManagerInterface $eep_manager) {
    $this->eepManager = $eep_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('eep.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function alterRoutes(RouteCollection $collection) {
    if ($this->eepManager->isPasswordResetEnabled() && $route = $collection->get('user.pass')) {
      $route->setDefault('_form', '\Drupal\eep\Form\UserPasswordForm');
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents(): array {
    $events = parent::getSubscribedEvents();

    $events[RoutingEvents::ALTER] = ['onAlterRoutes', -1000];

    return $events;
  }

}
