<?php

namespace Drupal\eep;

use Drupal\Core\Mail\MailManagerInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\Utility\Token;

/**
 * EepManager service.
 */
class EepManager implements ContainerInjectionInterface, EepManagerInterface {
  use StringTranslationTrait;

  /**
   * Mail manager service.
   *
   * @var \Drupal\Core\Mail\MailManagerInterface
   */
  protected $mailManager;

  /**
   * Config factory service.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * Language manager service.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  protected $languageManager;

  /**
   * Token service.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  protected $tokenService;

  /**
   * Constructs an EepManager object.
   *
   * @param \Drupal\Core\Mail\MailManagerInterface $mail_manager
   *   Mail manager service.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   Config factory service.
   * @param \Drupal\Core\Language\LanguageManagerInterface $language_manager
   *   Config factory service.
   * @param \Drupal\Core\Utility\Token $token_service
   *   Token service.
   */
  public function __construct(MailManagerInterface $mail_manager, ConfigFactoryInterface $config_factory, LanguageManagerInterface $language_manager, Token $token_service) {
    $this->mailManager = $mail_manager;
    $this->configFactory = $config_factory;
    $this->languageManager = $language_manager;
    $this->tokenService = $token_service;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    // Instantiates this form class.
    return new static(
      // Load the service required to construct this class.
      $container->get('plugin.manager.mail'),
      $container->get('config.factory'),
      $container->get('language_manager'),
      $container->get('token')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function isUserRegisterEnabled(): bool {
    $settings = $this->configFactory->getEditable("eep.settings");
    $value = $settings->get('enable_user_register');
    return is_null($value) ? FALSE : $value;
  }

  /**
   * {@inheritdoc}
   */
  public function isPasswordResetEnabled(): bool {
    $settings = $this->configFactory->getEditable("eep.settings");
    $value = $settings->get('enable_password_reset');
    return is_null($value) ? FALSE : $value;
  }

  /**
   * {@inheritdoc}
   */
  public function sendCustomResetMail($user) {
    $settings = $this->configFactory->getEditable("eep.settings");
    $langcode = $this->languageManager->getCurrentLanguage()->getId();
    $token_options = [
      'langcode' => $langcode,
      'callback' => 'user_mail_tokens',
      'clear' => TRUE,
    ];

    $params['subject'] = $this->tokenService->replace(
      $settings->get('subject_user_register'),
      ['user' => $user],
      $token_options,
    );

    $params['message'] = $this->tokenService->replace(
      $settings->get('email_user_register'),
      ['user' => $user],
      $token_options,
    );

    $this->mailManager->mail('eep',
      'eep_reset_password',
      $user->{'mail'}->value,
      $langcode,
      $params,
      FALSE,
      TRUE
    );
  }

}
