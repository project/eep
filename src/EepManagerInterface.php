<?php

namespace Drupal\eep;

/**
 * EepManagerInterface interface.
 */
interface EepManagerInterface {

  /**
   * Returns TRUE if the user register alteration is active in configuration.
   *
   * @return bool
   *   TRUE if the user register alteration is active in configuration.
   */
  public function isUserRegisterEnabled(): bool;

  /**
   * Returns TRUE if the password reset alteration is active in configuration.
   *
   * @return bool
   *   TRUE if the password reset alteration is active in configuration.
   */
  public function isPasswordResetEnabled(): bool;

}
