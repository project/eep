<?php

namespace Drupal\eep\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure EMail enumeration prevention settings for this site.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'eep_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['eep.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['tokens'] = [
      '#theme' => 'token_tree_link',
      '#token_types' => ['node'],
      '#global_types' => TRUE,
      '#click_insert' => TRUE,
      '#show_restricted' => FALSE,
      '#recursion_limit' => 3,
      '#text' => $this->t('Browse available tokens'),
    ];

    $form['enable_user_register'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable on the user register form'),
      '#default_value' => $this->config('eep.settings')->get('enable_user_register'),
    ];

    $form['subject_user_register'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Custom subject for user register form'),
      '#default_value' => $this->config('eep.settings')->get('subject_user_register'),
    ];

    $form['email_user_register'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Custom email for user register form'),
      '#description' => $this->t('This email will be sent when a user tries to register a new user with an existing email. YOu may use tokens.'),
      '#default_value' => $this->config('eep.settings')->get('email_user_register'),
    ];

    $form['enable_password_reset'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable on the password reset form'),
      '#default_value' => $this->config('eep.settings')->get('enable_password_reset'),
    ];

    $form['message_password_reset'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Custom message for password reset form'),
      '#default_value' => $this->config('eep.settings')->get('message_password_reset'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('eep.settings')
      ->set('enable_user_register', $form_state->getValue('enable_user_register'))
      ->set('subject_user_register', $form_state->getValue('subject_user_register'))
      ->set('email_user_register', $form_state->getValue('email_user_register'))
      ->set('enable_password_reset', $form_state->getValue('enable_password_reset'))
      ->set('message_password_reset', $form_state->getValue('message_password_reset'))
      ->save();
    parent::submitForm($form, $form_state);
  }

}
