<?php

namespace Drupal\eep\Form;

use Drupal\Core\Form\FormStateInterface;
use Drupal\user\RegisterForm as BaseRegisterForm;
use Drupal\user\UserInterface;
use Drupal\Core\Entity\EntityConstraintViolationListInterface;
use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Entity\EntityRepositoryInterface;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\Validation\Plugin\Validation\Constraint\UniqueFieldConstraint;
use Drupal\eep\EepManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Form handler for the user register forms.
 *
 * @internal
 */
class RegisterForm extends BaseRegisterForm {

  /**
   * The email enumeration prevention manager service.
   *
   * @var \Drupal\eep\EepManagerInterface
   */
  protected $eepManager;

  /**
   * Constructs a new RegisterForm object.
   *
   * @param \Drupal\Core\Entity\EntityRepositoryInterface $entity_repository
   *   The entity repository.
   * @param \Drupal\Core\Language\LanguageManagerInterface $language_manager
   *   The language manager.
   * @param \Drupal\eep\EepManagerInterface $eep_manager
   *   The language manager.
   * @param \Drupal\Core\Entity\EntityTypeBundleInfoInterface $entity_type_bundle_info
   *   The entity type bundle service.
   * @param \Drupal\Component\Datetime\TimeInterface $time
   *   The time service.
   */
  public function __construct(EntityRepositoryInterface $entity_repository, LanguageManagerInterface $language_manager, EepManagerInterface $eep_manager, EntityTypeBundleInfoInterface $entity_type_bundle_info = NULL, TimeInterface $time = NULL) {
    parent::__construct($entity_repository, $language_manager, $entity_type_bundle_info, $time);
    $this->eepManager = $eep_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity.repository'),
      $container->get('language_manager'),
      $container->get('eep.manager'),
      $container->get('entity_type.bundle.info'),
      $container->get('datetime.time'),
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function flagViolations(EntityConstraintViolationListInterface $violations, array $form, FormStateInterface $form_state) {
    if (!$this->currentUser()->hasPermission('access user profiles')) {
      foreach ($violations->getByFields(['mail', 'name']) as $offset => $violation) {
        if($violation->getConstraint() instanceof UniqueFieldConstraint) {
          $violations->remove($offset);
          $form_state->setValue('eep_event_occour', TRUE);
        }   
      }
    }
    parent::flagViolations($violations, $form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function buildEntity(array $form, FormStateInterface $form_state) {
    $eep_event_occour = $form_state->getValue('eep_event_occour');
    if (!$eep_event_occour) {
      return parent::buildEntity($form, $form_state);
    }
    $mail = $form_state->getValue('mail');
    $users = $this->entityTypeManager->getStorage('user')->loadByProperties(['mail' => $mail]);
    return reset($users);
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $eep_event_occour = $form_state->getValue('eep_event_occour');
    if (!$eep_event_occour) {
      parent::save($form, $form_state);
    }
    else {
      $this->eepManager->sendCustomResetMail($this->entity);

      // Display the same messages that come from core to avoid
      // giving hints that something different happened.
      switch ($this->config('user.settings')->get('register')) {
        case UserInterface::REGISTER_VISITORS_ADMINISTRATIVE_APPROVAL:
          $this->messenger()->addStatus($this->t('Thank you for applying for an account. Your account is currently pending approval by the site administrator.<br />In the meantime, a welcome message with further instructions has been sent to your email address.'));
          break;
        default:
          $this->messenger()->addStatus($this->t('A welcome message with further instructions has been sent to your email address.'));
          break;
      }

      $form_state->setRedirect('<front>');
    }
  }

}
