<?php

namespace Drupal\eep\Form;

use Drupal\Core\Form\FormStateInterface;
use Drupal\user\Form\UserPasswordForm as UserPasswordFormBase;

/**
 * UserPasswordForm class.
 */
class UserPasswordForm extends UserPasswordFormBase {

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $flood_config = $this->configFactory->get('user.flood');
    if (!$this->flood->isAllowed('user.password_request_ip', $flood_config->get('ip_limit'), $flood_config->get('ip_window'))) {
      $form_state->setErrorByName('name', $this->t('Too many password recovery requests from your IP address. It is temporarily blocked. Try again later or contact the site administrator.'));
      return;
    }
    $this->flood->register('user.password_request_ip', $flood_config->get('ip_window'));
    $name = trim($form_state->getValue('name'));
    // Try to load by email.
    $users = $this->userStorage->loadByProperties(['mail' => $name]);
    if (empty($users)) {
      // No success, try to load by name.
      $users = $this->userStorage->loadByProperties(['name' => $name]);
    }
    $account = reset($users);
    if ($account && $account->id()) {
      // Blocked accounts cannot request a new password.
      if ($account->isActive()) {
        // Register flood events based on the uid only, so they apply for any
        // IP address. This allows them to be cleared on successful reset (from
        // any IP).
        $identifier = $account->id();
        if (!$this->flood->isAllowed('user.password_request_user', $flood_config->get('user_limit'), $flood_config->get('user_window'), $identifier)) {
          return;
        }
        $this->flood->register('user.password_request_user', $flood_config->get('user_window'), $identifier);
        $form_state->setValueForElement(['#parents' => ['account']], $account);
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $langcode = $this->languageManager->getCurrentLanguage()->getId();

    $account = $form_state->getValue('account');
    if (!empty($account)) {
      $mail = _user_mail_notify('password_reset', $account, $langcode);
      if (!empty($mail)) {
        $this->logger('user')->notice('Password reset instructions mailed to %name at %email.', ['%name' => $account->getAccountName(), '%email' => $account->getEmail()]);
      }
    }
    else {
      $this->logger('user')->notice('Password reset requesto from non existing email');
    }
    $settings = $this->configFactory->getEditable('eep.settings');
    $this->messenger()->addStatus($settings->get('message_password_reset'));
    $form_state->setRedirect('user.page');
  }

}
